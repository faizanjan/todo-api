const joi = require("joi");

function validateTodo(input) {
  let schema = joi.object({
    name: joi.string().required(),
    checked: joi.boolean().required(),
    isImportant: joi.boolean().required()
  });
  return schema.validate(input);
}

function validateId(id) {
  return joi
    .object({
      id: joi.number().required(),
    })
    .validate(id);
}
module.exports = {validateTodo, validateId};

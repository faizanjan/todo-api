const express = require('express');
const router = require('./routes/todoRoutes');
require('dotenv').config();

const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5173');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

const handle500 = (req, res) => {
    res.status(500).send("<h1>Internal Server Error</h1>");
  };
  

app.use(express.json());
app.use('/todos', router);
app.use(handle500);

app.listen(process.env.PORT,(err)=>{
    if(err) console.error(err)
    else console.log("Server is up and running on port: ", process.env.PORT);
})
const express = require('express');
const router = express.Router();
const controller = require('./controllers')

router.get('/', controller.getAllTodos);
router.get('/:id', controller.getTodo);
router.post('/', controller.addTodo);
router.put('/:id', controller.updateTodo);
router.delete('/:id', controller.deleteTodo);

module.exports = router;
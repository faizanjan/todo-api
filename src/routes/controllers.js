const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const { validateTodo, validateId } = require("../validations");

async function getAllTodos(request, response, next) {
  try {
    const todos = await prisma.todos.findMany({
      orderBy: {
        id: "desc",
      },
    });
    return response.status(200).json(todos);
  } catch (error) {
    next(error);
  }
}

async function getTodo(request, response, next) {
  try {
    let id = parseInt(request.params.id);
    const idVldt = validateId({ id });
    if (idVldt.error) {
      return response.status(404).send(idVldt.error.details[0].message);
    }

    let todoItem = await prisma.todos.findUnique({
      where: {
        id,
      },
    });
    if (todoItem === null) {
      response.status(400).json({ message: "not found" });
    } else {
      response.status(200).json(todoItem);
    }
  } catch (error) {
    next(error);
  }
}

async function addTodo(request, response, next) {
  try {
    const { error } = validateTodo(request.body);
    if (error) {
      return response.status(400).send(error);
    }

    let newTodo = await prisma.todos.create({
      data: {
        id: request.body.id,
        name: request.body.name,
        checked: request.body.checked,
        isImportant: request.body.isImportant
      },
    });
    response.status(201).json(newTodo);
  } catch (error) {
    next(error);
  }
}

async function updateTodo(request, response, next) {
  try {
    let id = parseInt(request.params.id);
    const idVldt = validateId({ id });
    if (idVldt.error) {
      return response.status(404).send(idVldt.error.details[0].message);
    }

    const todoVldt = validateTodo(request.body);
    if (todoVldt.error) {
      return response.status(400).send(todoVldt.error.details[0].message);
    }

    const checkId = await prisma.todos.findUnique({ where: { id } });
    if (checkId === null) {
      return response.status(404).send("Id not found");
    }

    let updatedTodo = await prisma.todos.update({
      data: {
        todo: request.body.todo,
        checked: request.body.checked,
        isImportant : request.body.isImportant
      },
      where: {
        id,
      },
    });

    return response.status(200).json(updatedTodo);
  } catch (error) {
    next(error);
  }
}

async function deleteTodo(request, response, next) {
  try {
    let id = parseInt(request.params.id);
    const idVldt = validateId({ id });
    if (idVldt.error) {
      return response.status(404).send(idVldt.error.details[0].message);
    }

    const checkId = await prisma.todos.findUnique({ where: { id } });
    if (checkId === null) {
      return response.status(404).send("Please enter a valid Id");
    }

    let deletedTodo = await prisma.todos.delete({ where: { id } });
    return response.status(200).json(deletedTodo);
  } catch (error) {
    next(error);
  }
}

module.exports = { getAllTodos, getTodo, addTodo, updateTodo, deleteTodo };

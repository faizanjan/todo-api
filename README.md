# Express JS Project: todos CRUD API

This project is an API that allows CRUD (Create / Read / Update / Delete) operations on todos. It is built using Express.js, Postgres, Prisma ORM, and Joi validator.

## Installation

1. Clone the repository:

```bash
git clone git@gitlab.com:faizanjan/todo-api.git
```

2. Install the dependencies:

```bash
npm install
```

## Dependencies

- Express.js: ^4.18.2
- Prisma: ^4.14.0
- Joi: ^17.9.2
- pg: ^8.10.0
- dotenv: ^16.0.3

## Database

- Create a PostgreSQL database with the following schema:

```sql
CREATE TABLE todos (
    id SERIAL PRIMARY KEY,
    todo VARCHAR,
    isComplete BOOLEAN
);
```

- Update the `.env` file with your PostgreSQL database credentials:

```dotenv
DATABASE_URL=postgres://username:password@localhost:5432/database_name
```

- Run the migrations to generate the Prisma client:

```bash
npx prisma migrate dev
```

## Usage

Start the server by running the following command:

```bash
npm start
```

The server will run on `http://localhost:3000`.

## API Routes

- GET `/todos`: Retrieves all todos.

- GET `/todos/:id`: Retrieves a specific todo by its ID.

- POST `/todos`: Creates a new todo.

- PUT `/todos/:id`: Updates a todo with the specified ID.

- DELETE `/todos/:id`: Deletes a todo with the specified ID.

## Testing

You can test the API using a tool like Postman. Here's the Postman collection for the API:

[Postman Collection](https://api.postman.com/collections/27381901-30f25f3b-adb6-467c-9479-36f1f483ab8d?access_key=PMAT-01H0778GYMN2PVP7HXMKF77KB5)

## Status Codes

- GET /todos/:id - Return 404 if todo is not found, 200 on success

- POST /todos - Return 400 when todo is invalid, return 201 on success

- PUT /todos/:id - Return 404 if todo is not found, 400 when todo is invalid, and 200 on success

- DELETE /todos - Return 200


## Contributing

Contributions to this project are welcome. To contribute, follow these steps:

1. Fork the repository
2. Create a new branch
3. Make your changes
4. Open a pull request

Please make sure to follow the code style and include tests for any new features or bug fixes.

## Acknowledgments

- [Express.js](https://expressjs.com/)
- [Prisma](https://www.prisma.io/)
- [Joi](https://joi.dev/)
- [PostgreSQL](https://www.postgresql.org/)

